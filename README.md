# Log File Viewer

I needed a simple log viewer to use at debug time and prevent lots of openings, closings, and refreshings of log files.
So I wrote one. (Maybe I add extra features in the future)
In Microsoft Windows you can drag & drop log files or log folders on the output file. (Instead of providing arguments in command line)