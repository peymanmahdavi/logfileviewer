﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogFileViewer
{
    class Program
    {
        private static readonly string defaultAddress = "C:\\logs";
        private static readonly string fileNameFormat = "yyyy.MM.dd";
        private static readonly string fileNameExtension = ".log";

        static void Main(string[] args)
        {
            string theAddress = defaultAddress;
            bool AutoFileSwitching = true;

            if (args != null && args.Length > 0 && !string.IsNullOrWhiteSpace(args[0]))
            {
                string address = Path.GetFullPath(args[0]);
                if (Directory.Exists(address))
                    theAddress = address;
                if(File.Exists(address))
                {
                    AutoFileSwitching = false;
                    theAddress = address;
                }
            }

            while (true)
            {
                string fileAddress = theAddress;
                if (!fileAddress.Contains(fileNameExtension))
                    fileAddress += $"\\{DateTime.Now.ToString(fileNameFormat)}{fileNameExtension}";

                if (File.Exists(fileAddress))
                {
                    using (FileStream fs = File.Open(fileAddress, FileMode.Open, FileAccess.Read, FileShare.Delete))
                    {
                        while (true)
                        {
                            byte[] theByte = new byte[1];
                            theByte[0] = (byte)fs.ReadByte();
                            if ((theByte[0] > byte.MinValue) && (theByte[0] < byte.MaxValue))
                            {
                                string theString = Encoding.Default.GetString(theByte);
                                Console.Write(theString);
                            }
                            else
                            {
                                //If it reaches end of file, a little nap would be good for system.
                                System.Threading.Thread.Sleep(1000);
                            }

                            if (AutoFileSwitching)
                            {
                                //Check if the date has changed?
                                //If yes, break the loop and go for new file.
                                string dtDay = DateTime.Now.ToString(fileNameFormat);
                                if (!fileAddress.Contains(dtDay))
                                    break;
                            }
                        }
                    }
                }
                System.Threading.Thread.Sleep(10 * 1000);
            }
        }
    }
}
